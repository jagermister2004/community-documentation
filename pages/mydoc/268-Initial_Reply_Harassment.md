---
title: Initial Reply for Harassment Cases
keywords: email templates, initial reply, case handling policy, harassment, gendered online violence
last_updated: August 12, 2021
tags: [helpline_procedures_templates, harassment_templates, templates]
summary: "First response to clients who have been targeted by gendered online violence"
sidebar: mydoc_sidebar
permalink: 268-Initial_Reply_Harassment.html
folder: mydoc
conf: Public
ref: Initial_Reply_Harassment
lang: en
---


# Initial Reply for Harassment Cases
## First response to clients who have been targeted by gendered online violence

### Body

Dear {{ beneficiary name }},

Thank you for contacting the Access Now Digital Security Helpline (https://www.accessnow.org/help). My name is {{ incident handler name }}, and I am here to support you in understanding the problem that you are experiencing and to figure out what you want to do next.

We have received your initial request and I am working on it. To better support and respond to your needs, we need to ask a few questions and keep track of what has happened to follow up.

Please take you time to answer these questions:

- Would it be appropriate if we set up a call with you, either video or just audio?
- If you'd rather have someone speak in your behalf, please let me know. I'm ready to speak with someone you trust and who can provide details on your case, and can keep exchanging emails.
- [[ if the incident handler is not a woman ]] If you prefer to speak with a woman, please let me know and I will refer you to one of my colleagues.

Before we speak again I want to ask you to help me out by keeping record of what happens, saving important information and finding a trusted friend who can comfort you and give you a hand managing the incident you are experiencing.

Please reply to this message including “[accessnow #{{ ticket id }}]” in the subject line and
your answer to my questions.


Kind regards and stay strong,

{{ incident handler name }}


* * *


### Related Articles

- [Article #234: Online Harassment Targeting a Civil Society Member](234-FAQ-Online_Harassment.html)
